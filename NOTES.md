# Notes

- https://github.com/vuejs/core/discussions/9490#discussioncomment-7400626
- https://github.com/vuejs/core/blob/v3.4.13/packages/runtime-core/src/component.ts#L634
- https://github.com/vuejs/core/blob/v3.4.13/packages/runtime-core/src/component.ts#L265
- https://www.npmjs.com/package/flatted
- https://github.com/WebReflection/flatted
- https://github.com/WebReflection/flatted/tree/main/python
- https://vuejsbr-docs-next.netlify.app/api/composition-api.html#getcurrentinstance
- https://github.com/vuejs/vue/issues/12596#issuecomment-1173269807
- `console.log(JSON.stringify(instance));`: `Square.vue:7 Uncaught (in promise) TypeError: Converting circular structure to JSON`
- https://mokkapps.de/vue-tips/check-if-component-has-event-listener-attached
- https://github.com/vuejs/vue/issues/7349#issuecomment-380431978
- https://web.dev/articles/6-css-snippets-every-front-end-developer-should-know-in-2023#3_grid_pile
- https://modernfontstacks.com/

## Commands

```bash
npm install vue flatted && npm install -D vite @vitejs/plugin-vue typescript vue-tsc create-vite-tsconfigs sort-package-json npm-run-all2 prettier
```

```bash
rm -rf node_modules/ && npm install
```
