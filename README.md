# vue3-get-current-instance-demo

Use the `getCurrentInstance()` private function to check the internal structure of a component in Vue 3.

## Development

Install [fnm](https://github.com/Schniz/fnm) (if necessary).

```bash
fnm install && fnm use && node --version && npm --version
```

```bash
npm install
```

```bash
npm run dev
```

```bash
npm run lint
```

```bash
npm run format
```
